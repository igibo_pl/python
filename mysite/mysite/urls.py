from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers
from django.conf.urls import url
from api import views

router = routers.DefaultRouter()
router.register(r'brand', views.BrandViewSet)
router.register(r'model', views.ModelViewSet)
router.register(r'car', views.CarViewSet)

urlpatterns = [
    url(r'api/', include(router.urls)),
    re_path('^', include('polls.urls')),
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
