from django.db import models
import datetime

LIST_CURRENCY = (
    ('USD', 'USD'),
    ('EUR', 'EUR'),
    ('RUB', 'RUB'),
    ('PLN', 'PLN'),
)
YEARS_LIST = []
now = datetime.datetime.now()
for r in range(1985, now.year + 1):
    YEARS_LIST.append((r, r))


class Brand(models.Model):
    title = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Model(models.Model):
    title = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Car(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    year = models.IntegerField(choices=YEARS_LIST, default='2018')
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    currency = models.CharField(max_length=5, choices=LIST_CURRENCY, default='USD')
    owner_name = models.CharField(max_length=30)

    def get_category(self):
        if self.year < 1990:
            return 'До 1990 года выпуска'
        elif 1990 <= self.year < 2000:
            return 'От 1990 до 2000 года выпуска'
        elif 2000 <= self.year < 2010:
            return 'От 2000 до 2010 года выпуска'
        else:
            return 'После 2010 года выпуска'

    def __str__(self):
        return '%s %s %s %s %s' % (self.brand.title, ':', self.year, ':', self.owner_name,)
