from django.shortcuts import render
from .models import Car


def index(request):
    filter_params = dict()
    search = request.GET.get('search')
    order = request.GET.get('order')
    cars_objects = Car.objects

    if search is not None:
        filter_params.update({'brand__contains': search})
    if order is not None:
        cars_objects = cars_objects.order_by(order)

    cars = cars_objects.filter(**filter_params).all()
    return render(request, "polls/home.html", {"cars": cars, })
