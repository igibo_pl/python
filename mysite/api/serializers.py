from polls.models import Brand, Model, Car
from rest_framework import serializers


class CarSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk')
    brand = serializers.CharField(required=False, allow_blank=True, max_length=10)
    model = serializers.CharField(required=False, allow_blank=True, max_length=10)
    year = serializers.IntegerField(write_only=True, default='USD')
    price = serializers.DecimalField(max_digits=10, decimal_places=2, default=0)
    currency = serializers.CharField(required=False, allow_blank=True, max_length=10)
    owner_name = serializers.CharField(required=False, allow_blank=True, max_length=30)

    class Meta:
        model = Car
        fields = ('id', 'brand', 'model', 'year', 'price', 'currency', 'owner_name')


class BrandSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk')
    title = serializers.CharField(required=False, allow_blank=True, max_length=10)

    class Meta:
        model = Brand
        fields = ('id', 'title')


class ModelSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='pk')
    title = serializers.CharField(required=False, allow_blank=True, max_length=10)

    class Meta:
        model = Model
        fields = ('id', 'title')
