from django.shortcuts import render
from polls.models import Brand, Model, Car
from rest_framework import viewsets
from .serializers import BrandSerializer, ModelSerializer, CarSerializer


class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all().order_by('-id')
    serializer_class = CarSerializer


class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all().order_by('-title')
    serializer_class = BrandSerializer


class ModelViewSet(viewsets.ModelViewSet):
    queryset = Model.objects.all().order_by('-title')
    serializer_class = ModelSerializer